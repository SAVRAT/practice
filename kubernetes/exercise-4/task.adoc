== Работа с Deployment

. Создайте манифест `Deployment` (можете использовать файл `deployment.yaml` в этой директории)
.. имя `nginx-deployment`
.. количество реплик = 1
.. метки Pod = `app: nginx` и `tier: frontend`
.. образ `nginx:1.18.0-alpine`
.. port контейнера - `80`
. Примените созданный манифест
. Просмотрите список Deployment и найдите `nginx-deployment`
. Просмотрите список ReplicaSet, Pod и найдите связанные `nginx-deployment`
. Масштабируйте до количества реплик = 5
. Просмотрите список Deployment и найдите `nginx-deployment`
. Просмотрите список ReplicaSet, Pod и связанные с `nginx-deployment`
. Удалите `nginx-deployment` `Deployment`


https://kubernetes.io/docs/concepts/workloads/controllers/deployment/[Документация по Deployment.]

https://kubernetes.io/docs/reference/kubectl/cheatsheet/[kubectl cheat sheet.]


